# Contributor license and agreement

Thank you for your interest in contributing to Ratio DSM! To ensure the project can be distributed
under both the GNU General Public License version 3 or later (GPLv3) and a proprietary license for
those who wish to license the software under different terms, we require contributors to accept the
following terms:

## Dual licensing

Ratio DSM will be made available under both the GPLv3 as well as proprietary licenses. Users can
choose the license under which they wish to use the project. When the GPLv3 is chosen, the terms of
the GPLv3 will apply to the entire project, including all contributions. For proprietary licenses
granted by Ration Innovations B.V., terms regarding support and usage are agreed upon on a
case-by-case basis.

## Grant of rights

By contributing to Ratio DSM, you grant the project and Ratio Innovations B.V. a perpetual,
irrevocable, worldwide, non-exclusive, royalty-free, fully paid-up license to use, reproduce,
modify, distribute, and sublicense your contributions under both the GPLv3 as well as a proprietary
license as granted by Ratio Innovations B.V..

## Intellectual property representation

By making a contribution, you represent that you have the necessary rights to contribute the work
and that your contributions do not infringe any third-party intellectual property rights.

## Licensing compatibility

Contributions made under the proprietary license are also made available under the GPLv3. This
ensures that the project can be distributed under the terms of the GPLv3.

## Modification and relicensing

Ratio Innovations B.V. reserves the right to modify the project and to relicense the codebase under
different licenses, including future versions of the GPLv3 or a proprietary license. However,
contributions made under the GPLv3 will remain available under the terms of the GPLv3.

## Dispute resolution

Any disputes arising between contributors and Ratio Innovations B.V. shall be resolved through
mediation or arbitration in accordance with Dutch law.

## Acceptance and agreement

By contributing to Ratio DSM, you explicitly accept and agree to the terms of this agreement.

## Severability

If any provision of this agreement is found to be unenforceable, it shall not affect the validity
and enforceability of the remaining provisions.
