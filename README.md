# Ratio DSM plotting

Ratio Dependency Structure Matrix plotting library.

## Changelog

This repository keeps a [CHANGELOG.md](./CHANGELOG.md) according to the recommendations by
[Keep a Changelog](https://keepachangelog.com/).

## Contributions and license

To get contributing, feel free to fork, pick up an issue or file your own and get going for your
first merge! We'll be more than happy to help. The contributor agreement can be found in the
[CONTRIBUTING](./CONTRIBUTING.md) file.

You might have noticed that this crate follows the "GPL-3.0-or-later" license and some of our other
crates follow the "MIT-OR-Apache-2.0" license. This is intentional. Some crates, we consider as
"just utilities" that we would like to offer to the entire Rust community without any question asked
or guarantees given. Others like Ratio DSM are more closely related to our core business, being
Systems Engineering and dependency analysis and is therefore licensed following a dual licensing
model. In short, we want to provide anyone that wishes to use our published software under the GNU
GPLv3 to do so freely and without any further limitation. The GNU GPLv3 is a strong copyleft license
that promotes the distribution of free, open-source software. In that spirit, it requires dependent
pieces of software to follow the same route. This might be too restrictive for some. To accommodate
users with specific requirements regarding licenses, we offer a proprietary license. The terms can
be discussed by reaching out to Ratio.
