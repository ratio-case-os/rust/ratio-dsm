# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.1] - 2024-08-16

### Fixed

- Highlight bands not showing properly for coordinates.

## [0.8.0] - 2024-08-16

### Changed

- Restructured settings objects for more conventional use.

## [0.7.2] - 2024-08-12

### Changed

- Adopted `ratio-matrix` 0.4.0.

## [0.7.1] - 2024-08-12

### Changed

- Adopted `ratio-matrix` 0.3.0.

## [0.7.0] - 2024-08-10

### Changed

- Removed the `data` module in favor of the new types and traits from the `ratio-matrix` crate.

## [0.6.2] - 2024-08-09

### Added

- Add Dimensions2D implementation for (usize, usize).

## [0.6.1] - 2024-08-09

### Fixed

- Account for the currently sent grid line width when calculating the viewbox.

## [0.6.0] - 2024-08-07

### Changed

- Changed the matrix traits to be more flexible and added a built-in `CellReference` struct that
  uses it to access the underlying data in a lightweight fashion.
- Renamed the `PiechartComponent` to `PiechartsComponent` for consistency as there will probably
  come a single piechart component in the future.

### Fixed

- The `ClustersComponent` now makes proper use of its classname.

## [0.5.0] - 2024-08-07

### Changed

- Relieved some of the trait requirements on the 2D matrix types and adjusted methods accordingly.

## [0.4.1] - 2024-08-07

### Fixed

- Removed superfluous dimensioning requirements on highlights.

## [0.4.0] - 2024-08-07

### Changed

- Better matrix slicing for the built-in data types.
- Introduce dimensions arguments to relevant components to split trait functionality.
- Updated some documentation.

## [0.3.0] - 2024-08-07

### Changed

- Composition of different highlight layers was still slightly too cumbersome and expected to have
  multiple potential sources. Adjusted the components and traits accordingly.

## [0.2.0] - 2024-08-07

### Changed

- Highlight data model proved difficult to work with. Switched to locations and a settings override
  coming directly from the `Highlights` trait.

## [0.1.0] - 2024-08-06

### Added

- Initial version!
