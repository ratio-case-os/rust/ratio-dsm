set windows-shell := ["powershell.exe", "-NoLogo", "-Command"]

# List all jobs by default.
default:
  @just --list

# Lint both the library and dependencies.
lint fix="": (lint-lib fix)  lint-deny

# Lint the Rust code.
lint-lib fix="":
  cargo clippy --all-targets --color always {{ if fix != "" { "--allow-dirty --allow-staged --fix" } else { "" } }}
  {{ if fix != "" { "cargo +nightly fmt" } else { "" } }}

# Do fixups where we can.
fix: (lint-lib "y")

# Lint project dependencies and licenses
lint-deny:
  cargo deny check

# Run library tests continuously.
test:
  bacon -j test

# Run library tests with coverage.
coverage:
  cargo tarpaulin --tests --doc --color always --locked --out Xml --output-dir target/tarpaulin
