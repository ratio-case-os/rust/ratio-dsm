//! Settings data

use std::collections::BTreeMap;
use std::rc::Rc;

use palette::Srgba;
use ratio_color::{Categorical, Container, LinearGradient, Numerical, Palette};

/// DSM plotting settings.
///
/// Different components are stored in Rc's for ease-of-use while replacing parts interactively.
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct DsmSettings {
    /// Grid settings.
    pub grid: Rc<GridSettings>,
    /// Font settings.
    pub font: Rc<FontSettings>,
    /// Piechart/Wedge settings.
    pub pie: Rc<PieSettings>,
    /// Color palette settings.
    pub palette: Rc<PaletteSettings>,
    /// Weight domains.
    pub domains: Rc<BTreeMap<String, (f64, f64)>>,
}

/// Settings for the background grid.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct GridSettings {
    /// Grid cell size.
    pub size: usize,
    /// Grid line width.
    pub line_width: f64,
    /// Grid line color.
    pub line_color: Srgba<u8>,
    /// Whether to draw a diagonal.
    pub diagonal: bool,
    /// Kind line color.
    pub split_color: Srgba<u8>,
    /// Kind line stroke type.
    pub split_dash: Vec<u8>,
    /// Cluster line color.
    pub cluster_color: Srgba<u8>,
    /// Bus cluster dash.
    pub special_dash: Vec<u8>,
    /// Grid background color.
    pub background_color: Srgba<u8>,
}
impl Default for GridSettings {
    fn default() -> Self {
        Self {
            size: 30,
            line_width: 1.0,
            line_color: Srgba::new(191, 191, 202, 255),
            diagonal: true,
            split_color: Srgba::new(132, 132, 153, 255),
            split_dash: vec![10, 10],
            cluster_color: Srgba::new(72, 72, 104, 255),
            special_dash: vec![5, 5],
            background_color: Srgba::new(255, 255, 255, 255),
        }
    }
}

/// Font settings to re-use.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct FontSettings {
    /// Font factor with respect to grid size.
    pub font_factor: f64,
}
impl Default for FontSettings {
    fn default() -> Self {
        Self { font_factor: 0.32 }
    }
}

/// Piechart/Wedge rendering settings.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct PieSettings {
    /// Wedge radius as a fraction of the cell.
    pub radius: f64,
    /// Wedge stroke width in pixels.
    pub stroke_width: f64,
    /// Wedge stroke color.
    pub stroke_color: Srgba<u8>,
    /// Wedge division mode.
    pub division: PieDivisionMode,
    /// Wedge display mode.
    pub display: PieDisplayMode,
}
impl Default for PieSettings {
    fn default() -> Self {
        Self {
            radius: 0.4,
            stroke_width: 0.5,
            stroke_color: Srgba::new(255, 255, 255, 255),
            division: Default::default(),
            display: Default::default(),
        }
    }
}

/// How to divide the wedges in each piechart.
#[derive(Copy, Clone, Default, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(test, derive(strum::EnumIter))]
pub enum PieDivisionMode {
    /// Divide all present wedges equally.
    #[default]
    Equal,
    /// Assign a fixed position and angle of the pie to each selected field.
    Fixed,
    /// Resize all wedges according to their absolute value.
    Relative,
}

/// How to interpret the fields for each piechart.
#[derive(Copy, Clone, Default, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(test, derive(strum::EnumIter))]
pub enum PieDisplayMode {
    /// The presence of any value is drawn as a full circle.
    #[default]
    Binary,
    /// Interpret field values as categorical occurrence.
    Categorical,
    /// Interpret field values as numerical weights.
    Numerical,
}

/// Color palette to use while plotting.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct PaletteSettings {
    /// Binary color.
    pub binary: Srgba<u8>,
    /// Categorical palette.
    pub categorical: Rc<CategoricalPalette>,
    /// Numerical palette.
    pub numerical: Rc<NumericalPalette>,
}
impl Default for PaletteSettings {
    fn default() -> Self {
        Self {
            binary: Srgba::new(99, 110, 250, 255),
            categorical: Palette::with_preset(&Categorical::default(), Default::default()).into(),
            numerical: Palette::with_presets(
                &[Numerical::default(), Numerical::DivGeyser],
                Default::default(),
            )
            .into(),
        }
    }
}

/// Categorical palette mapping field names to RGBA colors.
pub type CategoricalPalette = Palette<String, Srgba<u8>>;
/// Numerical palette mapping field names to linear gradients.
pub type NumericalPalette = Palette<String, LinearGradient>;

/// Highlight settings for use with the Highlights component and trait. Not included in the global
/// settings since it is common to have many different types of highlights in a figure.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct HighlightSettings {
    /// Color of the highlight bands.
    pub area: Option<Srgba<u8>>,
    /// Halo or cursor settings.
    pub halo: Option<HaloSettings>,
}
impl Default for HighlightSettings {
    fn default() -> Self {
        Self {
            area: Some(Srgba::new(130, 163, 254, 64)),
            halo: Some(HaloSettings::default()),
        }
    }
}

/// Settings to render "halos" around piecharts.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct HaloSettings {
    /// Halo radius as the fraction of a cell.
    pub radius: f64,
    /// Halo stroke color.
    pub color: Srgba<u8>,
    /// Halo stroke width.
    pub width: f64,
    /// Halo dash if applicable.
    pub dash: Option<Vec<u8>>,
}
impl Default for HaloSettings {
    fn default() -> Self {
        Self {
            color: Srgba::new(255, 152, 0, 255),
            radius: 0.45,
            width: 2.0,
            dash: None,
        }
    }
}
