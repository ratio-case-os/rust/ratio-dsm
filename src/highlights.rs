/// Highlight plotting
use palette::Srgba;
use ratio_matrix::traits::{Dimensions2D, Domain2D};
use ratio_matrix::MatrixLocation;
use svg::node::element::{Circle, Definitions, Group, Rectangle, Use};
use svg::Node;

use crate::settings::{DsmSettings, HaloSettings, HighlightSettings};
use crate::traits::{Component, Highlights};
use crate::utils::{dasharray, with_prefix};

/// Highlights plot component.
#[derive(Clone, Debug)]
pub struct HighlightsComponent<'a, D>
where
    D: Highlights,
{
    /// Highlights to draw.
    data: &'a D,
    /// Highlight settings to use.
    settings: HighlightSettings,
    /// Grid size in pixels.
    grid_size: usize,
    /// Length in cells for each row highlight.
    width: usize,
    /// Length in cells for each column highlight.
    height: usize,
    /// SVG id for the row to repeat.
    row_id: String,
    /// SVG id for the column to repeat.
    col_id: String,
    /// SVG id for the halo to repeat.
    halo_id: String,
    /// Area style classname.
    area_class: String,
    /// Whether to draw shaded areas.
    draw_areas: bool,
    /// Whether to draw halos.
    draw_halos: bool,
    /// Whether these highlights include any row.
    has_row: bool,
    /// Whether these highlights include any column.
    has_col: bool,
    /// Whether these highlights include any area.
    has_area: bool,
    /// Whether these highlights include any point.
    has_point: bool,
}
impl<'a, D> HighlightsComponent<'a, D>
where
    D: Highlights,
{
    /// Create a new highlight component based on a data source that exposes the `Highlights` trait.
    /// It is assumed that the coordinates are already adjusted for any restrictions on the viewing
    /// domain, but the dimensions for typical full row and column highlights are fetched from the
    /// respective function argument.
    pub fn new<Dim: Dimensions2D>(
        prefix: Option<&str>,
        data: &'a D,
        dimensions: &Dim,
        settings: &'a DsmSettings,
    ) -> Self {
        let grid_size = settings.grid.size;
        let width = dimensions.n_cols() * settings.grid.size;
        let height = dimensions.n_rows() * settings.grid.size;

        let row_id = with_prefix("hl-r", prefix);
        let col_id = with_prefix("hl-c", prefix);
        let halo_id = with_prefix("hl-h", prefix);
        let area_class = with_prefix("hl", prefix);

        let settings = data.settings().cloned().unwrap_or_default();
        let draw_areas = settings.area.is_some();
        let draw_halos = settings.halo.is_some();

        let locations = data.locations();
        let has_row = locations
            .iter()
            .any(|loc| matches!(loc, MatrixLocation::Row(..)));
        let has_col = locations
            .iter()
            .any(|loc| matches!(loc, MatrixLocation::Col(..)));
        let has_area = locations
            .iter()
            .any(|loc| matches!(loc, MatrixLocation::Range(..)));
        let has_point = locations
            .iter()
            .any(|loc| matches!(loc, MatrixLocation::Coordinates(..)));

        Self {
            data,
            settings,
            grid_size,
            width,
            height,
            row_id,
            col_id,
            halo_id,
            area_class,
            draw_areas,
            draw_halos,
            has_row,
            has_col,
            has_area,
            has_point,
        }
    }
    /// Create a new row use element.
    fn use_row(&self, pos: usize) -> Use {
        Use::new()
            .set("href", format!("#{}", self.row_id))
            .set("y", pos * self.grid_size)
    }
    /// Create a new column use element.
    fn use_col(&self, pos: usize) -> Use {
        Use::new()
            .set("href", format!("#{}", self.col_id))
            .set("x", pos * self.grid_size)
    }
    /// Create a new halo use element.
    fn use_halo(&self, row: usize, col: usize) -> Use {
        Use::new()
            .set("href", format!("#{}", self.halo_id))
            .set("x", col * self.grid_size)
            .set("y", row * self.grid_size)
    }
}

impl<'a, D> Component for HighlightsComponent<'a, D>
where
    D: Highlights,
{
    fn group(&self) -> Option<Group> {
        let mut group = Group::new();

        let locations = self.data.locations();
        if locations.is_empty() {
            return None;
        }

        for loc in locations.iter() {
            match loc {
                MatrixLocation::Coordinates(coordinates) => {
                    if self.draw_areas {
                        group.append(self.use_row(coordinates.row));
                        group.append(self.use_col(coordinates.col));
                    }
                    if self.draw_halos {
                        group.append(self.use_halo(coordinates.row, coordinates.col));
                    }
                }
                MatrixLocation::Row(row) => {
                    if self.draw_areas {
                        group.append(self.use_row(*row));
                    }
                }
                MatrixLocation::Col(col) => {
                    if self.draw_areas {
                        group.append(self.use_col(*col));
                    }
                }
                MatrixLocation::Range(range) => {
                    if self.draw_areas {
                        group.append(area(&self.area_class, self.grid_size, range));
                    }
                }
            };
        }

        if group.get_children().is_empty() {
            None
        } else {
            Some(group)
        }
    }

    fn definitions(&self) -> Option<Definitions> {
        let mut defs = Definitions::new();

        if self.draw_areas {
            if let Some(color) = self.settings.area.as_ref() {
                if self.has_row || self.has_point {
                    defs.append(row_area(&self.row_id, self.grid_size, self.width, color));
                }
                if self.has_col || self.has_point {
                    defs.append(col_area(&self.col_id, self.grid_size, self.height, color));
                }
            }
        }

        if self.draw_halos {
            if let Some(settings) = self.settings.halo.as_ref() {
                if self.has_point {
                    defs.append(halo(&self.halo_id, self.grid_size, settings));
                }
            }
        }

        if defs.get_children().is_empty() {
            None
        } else {
            Some(defs)
        }
    }

    fn style(&self) -> Option<String> {
        if self.draw_areas && self.has_area {
            let color = self.settings.area?;
            Some(format!(".{}{{fill:#{:02x};}}", self.area_class, color))
        } else {
            None
        }
    }
}

/// Row area definition to re-use.
fn row_area(id: &str, grid_size: usize, width: usize, color: &Srgba<u8>) -> Rectangle {
    Rectangle::new()
        .set("id", id)
        .set("fill", format!("#{:02x}", color))
        .set("width", width)
        .set("height", grid_size)
}

/// Column area definition to re-use.
fn col_area(id: &str, grid_size: usize, height: usize, color: &Srgba<u8>) -> Rectangle {
    Rectangle::new()
        .set("id", id)
        .set("fill", format!("#{:02x}", color))
        .set("width", grid_size)
        .set("height", height)
}

/// Halo definition to re-use.
fn halo(id: &str, grid_size: usize, settings: &HaloSettings) -> Circle {
    let offset = 0.5 * grid_size as f64;
    let radius = settings.radius * grid_size as f64;

    let circle = Circle::new()
        .set("id", id)
        .set("cx", offset)
        .set("cy", offset)
        .set("r", radius)
        .set("fill", "transparent")
        .set("stroke", format!("#{:02x}", settings.color))
        .set("stroke-width", settings.width);

    match &settings.dash {
        Some(dashes) => circle.set("stroke-dasharray", dasharray(dashes)),
        None => circle,
    }
}

/// Draw a rectangular area with a class.
fn area<D: Domain2D>(class: &str, grid_size: usize, data: &D) -> Rectangle {
    let x = grid_size * data.col_range().start;
    let y = grid_size * data.row_range().start;
    let width = grid_size * data.n_cols();
    let height = grid_size * data.n_rows();
    Rectangle::new()
        .set("class", class)
        .set("x", x)
        .set("y", y)
        .set("width", width)
        .set("height", height)
}
