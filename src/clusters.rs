//! Clusters plotting

use palette::Srgba;
use ratio_matrix::traits::Dimensions2D;
use svg::node::element::{Group, Rectangle};
use svg::Node;

use crate::settings::DsmSettings;
use crate::traits::{Cluster, Clusters, Component};
use crate::utils::{dasharray, with_prefix};

/// Clusters plot component.
#[derive(Clone, Debug)]
pub struct ClustersComponent<'a, Cl>
where
    Cl: Cluster + Clone,
{
    /// Cluster data.
    clusters: &'a [Cl],
    /// Grid size in pixels.
    grid_size: usize,
    /// Line width to use for clusters in pixels.
    line_width: f64,
    /// Line color for clusters.
    line_color: Srgba<u8>,
    /// Line dash to use for special clusters.
    special_dash: &'a [u8],
    /// Cluster class name to re-use.
    cluster_class: String,
}
impl<'a, Cl> ClustersComponent<'a, Cl>
where
    Cl: Cluster + Clone,
{
    /// Create a new clusters component with given prefix and a data source that provides the
    /// `Clusters` trait which exposes multiple cluster coordinates and information.
    ///
    /// Note that cluster ranges and coordinates are used as-is and won't be adjusted for a viewing
    /// range, which means that should be already taken care of in your trait implementation.
    pub fn new<D>(prefix: Option<&str>, data: &'a D, settings: &'a DsmSettings) -> Self
    where
        D: Clusters<Cl>,
    {
        Self {
            clusters: data.clusters(),
            grid_size: settings.grid.size,
            line_width: settings.grid.line_width,
            line_color: settings.grid.cluster_color,
            special_dash: &settings.grid.special_dash,
            cluster_class: with_prefix("cl", prefix),
        }
    }
}
impl<'a, C> Component for ClustersComponent<'a, C>
where
    C: Cluster + Clone,
{
    fn group(&self) -> Option<Group> {
        if self.clusters.is_empty() {
            return None;
        }

        let mut group = Group::new().set("class", self.cluster_class.as_ref());

        for c in self.clusters.iter() {
            group.append(cluster(c, self.grid_size));
        }

        Some(group)
    }
    fn style(&self) -> Option<String> {
        Some(format!(
            r##".{}{{fill:transparent;stroke-width:{}px;stroke:#{:02x}}}.special{{stroke-dasharray:{}}}"##,
            self.cluster_class,
            self.line_width,
            self.line_color,
            dasharray(self.special_dash)
        ))
    }
}

/// Draw a single cluster.
pub fn cluster<D>(data: &D, grid_size: usize) -> Rectangle
where
    D: Cluster,
{
    let x = grid_size * data.col_range().start;
    let y = grid_size * data.row_range().start;
    let width = grid_size * data.n_cols();
    let height = grid_size * data.n_rows();
    let rect = Rectangle::new()
        .set("x", x)
        .set("y", y)
        .set("width", width)
        .set("height", height);
    if data.is_special() {
        rect.set("class", "special")
    } else {
        rect
    }
}
