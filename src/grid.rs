//! Grid plotting

use palette::Srgba;
use ratio_matrix::traits::{Dimensions2D, Domain2D};
use ratio_matrix::{MatrixCoordinates, MatrixDimensions};
use svg::node::element::{Definitions, Group, Line, Rectangle, Use};
use svg::Node;

use crate::settings::DsmSettings;
use crate::traits::Component;
use crate::utils::with_prefix;

/// Grid plot component.
#[derive(Clone, Debug)]
pub struct GridComponent {
    size: usize,
    dimensions: MatrixDimensions,
    offset: MatrixCoordinates,
    width: usize,
    height: usize,
    stroke: Srgba<u8>,
    stroke_width: f64,
    diagonal: bool,
    row_id: String,
    col_id: String,
    diag_id: String,
}
impl GridComponent {
    /// Create a new grid component.
    pub fn new<D>(prefix: Option<&str>, data: &D, settings: &DsmSettings) -> Self
    where
        D: Domain2D,
    {
        let size = settings.grid.size;
        let dimensions = data.dimensions();
        let offsets = data.start();
        let width = dimensions.n_cols * size;
        let height = dimensions.n_rows * size;
        Self {
            size,
            dimensions,
            offset: offsets,
            width,
            height,
            stroke: settings.grid.line_color,
            stroke_width: settings.grid.line_width,
            diagonal: settings.grid.diagonal,
            row_id: with_prefix("grid-r", prefix),
            col_id: with_prefix("grid-c", prefix),
            diag_id: with_prefix("grid-d", prefix),
        }
    }
}

impl Component for GridComponent {
    fn group(&self) -> Option<Group> {
        let mut group = Group::new().add(
            Rectangle::new()
                .set("x", 0)
                .set("y", 0)
                .set("width", self.width)
                .set("height", self.height)
                .set("fill", "none")
                .set("stroke", format!("#{:02x}", self.stroke))
                .set("stroke-width", self.stroke_width),
        );

        // Horizontal lines.
        for r in 1..self.dimensions.n_rows {
            group.append(
                Use::new()
                    .set("href", format!("#{}", self.row_id))
                    .set("y", r * self.size),
            );
        }

        // Vertical lines.
        for c in 1..self.dimensions.n_cols {
            group.append(
                Use::new()
                    .set("href", format!("#{}", self.col_id))
                    .set("x", c * self.size),
            );
        }

        if self.diagonal {
            let lower = std::cmp::max(self.offset.row, self.offset.col);
            let upper = std::cmp::min(
                self.offset.row + self.dimensions.n_rows,
                self.offset.col + self.dimensions.n_cols,
            );
            for i in lower..upper {
                let x = (i - self.offset.col) * self.size;
                let y = (i - self.offset.row) * self.size;
                group.append(
                    Use::new()
                        .set("href", format!("#{}", self.diag_id))
                        .set("x", x)
                        .set("y", y),
                );
            }
        }

        Some(group)
    }

    fn definitions(&self) -> Option<Definitions> {
        // Re-usable pattern lines.
        let mut defs = Definitions::new()
            .add(
                Line::new()
                    .set("id", self.row_id.as_ref())
                    .set("x0", 0)
                    .set("x1", self.width)
                    .set("y0", 0)
                    .set("y1", 0)
                    .set("stroke-width", self.stroke_width)
                    .set("stroke", format!("#{:02x}", self.stroke)),
            )
            .add(
                Line::new()
                    .set("id", self.col_id.as_ref())
                    .set("x0", 0)
                    .set("x1", 0)
                    .set("y0", 0)
                    .set("y1", self.height)
                    .set("stroke-width", self.stroke_width)
                    .set("stroke", format!("#{:02x}", self.stroke)),
            );
        if self.diagonal {
            defs.append(
                Rectangle::new()
                    .set("id", self.diag_id.as_ref())
                    .set("width", self.size)
                    .set("height", self.size)
                    .set("fill", format!("#{:02x}", self.stroke)),
            );
        }

        Some(defs)
    }
    fn style(&self) -> Option<String> {
        None
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use ratio_matrix::traits::Matrix2D;
    use ratio_matrix::Matrix;

    #[allow(unused_imports)]
    use super::*;
    use crate::settings::DsmSettings;
    use crate::traits::GridViewbox;
    use crate::utils::test::write_to_examples;

    #[test]
    fn test_grid() {
        let data = Matrix::<()>::new(vec![(); 12], 4).expect("data");
        let settings = DsmSettings::default();

        let grid = GridComponent::new(None, &data, &settings);

        let doc = grid.document(Some(data.viewbox(&settings.grid)));

        write_to_examples("grid/grid.svg", &doc);
        assert_str_eq!(
            doc.to_string(),
            r##"<svg height="100%" viewBox="-0.5 -0.5 121 91" width="100%" xmlns="http://www.w3.org/2000/svg">
<defs>
<line id="grid-r" stroke="#bfbfcaff" stroke-width="1" x0="0" x1="120" y0="0" y1="0"/>
<line id="grid-c" stroke="#bfbfcaff" stroke-width="1" x0="0" x1="0" y0="0" y1="90"/>
<rect fill="#bfbfcaff" height="30" id="grid-d" width="30"/>
</defs>
<g>
<rect fill="none" height="90" stroke="#bfbfcaff" stroke-width="1" width="120" x="0" y="0"/>
<use href="#grid-r" y="30"/>
<use href="#grid-r" y="60"/>
<use href="#grid-c" x="30"/>
<use href="#grid-c" x="60"/>
<use href="#grid-c" x="90"/>
<use href="#grid-d" x="0" y="0"/>
<use href="#grid-d" x="30" y="30"/>
<use href="#grid-d" x="60" y="60"/>
</g>
</svg>"##
        );
    }
}
