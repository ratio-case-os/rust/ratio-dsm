//! Ratio's DSM plotting library
//!
//! This crate enables the plotting or drawing of Dependency Structure Matrices as SVG figures. It
//! features a a trait-based approach which enables you to keep your data as much as-is as possible
//! and only requires the bare minimum to be exposed using primarily Rust primitives.
//!
//! Take a look at the [`traits::Component`] trait for the typical structure of an SVG component and
//! the [`traits::Dsm`] for a single trait that on implementation allows for the
//! [`dsm::DsmComponent`] to be constructed.

pub mod clusters;
pub mod dsm;
pub mod grid;
pub mod highlights;
pub mod pie;
pub mod settings;
pub mod splits;
pub mod traits;
pub mod utils;
