//! Utilities

use std::fmt::Display;
use std::fs::{create_dir_all, OpenOptions};
use std::io::Write;
use std::path::Path;

/// Write a display object to file.
pub fn write_to_file<T: Display>(path: &str, object: &T) -> Result<(), std::io::Error> {
    let path = Path::new(path);
    if let Some(dir) = path.parent() {
        create_dir_all(dir)?;
    }
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(path)?;
    file.write_all(object.to_string().as_bytes())
}

/// Prefix a string if it's supplied.
pub fn with_prefix<S: AsRef<str>>(s: S, prefix: Option<S>) -> String {
    match prefix {
        Some(p) => format!("{}-{}", p.as_ref(), s.as_ref()),
        None => s.as_ref().to_string(),
    }
}

/// Build a string value for a dasharray.
pub fn dasharray(dashes: &[u8]) -> String {
    dashes
        .iter()
        .map(|&i| i.to_string())
        .collect::<Vec<_>>()
        .join(" ")
}

/// Random cell data.
#[cfg(test)]
pub mod test {
    use std::collections::BTreeMap;
    use std::ops::Range;

    use rand::prelude::*;
    use ratio_matrix::traits::{Domain2D, Matrix2D, MatrixRef};
    use ratio_matrix::{Matrix, MatrixCoordinates, MatrixLocation, MatrixRange};

    use super::*;
    use crate::settings::HighlightSettings;
    use crate::traits::{
        CellValue, Cluster, Clusters, Dsm, Highlights, IsSpecial, MatrixValue, SplitLines,
    };

    /// Write to example.
    pub fn write_to_examples<T: Display>(path: &str, object: &T) {
        write_to_file(&format!("./target/debug/examples/{}", path), object)
            .expect("write to target/debug/examples");
    }

    /// Just an empty struct.
    pub struct EmptyData;

    /// A random data generator with a preset seed. Whenever the next value
    pub struct RandomData;
    impl CellValue for RandomData {
        fn field_value<S: AsRef<str>>(&self, _field: &S) -> Option<f64> {
            if random::<f64>() > 0.1 {
                Some(random())
            } else {
                Some(0.0)
            }
        }
    }

    #[derive(Clone, Debug, Default)]
    pub struct TestData {
        pub matrix: TestMatrix,
        pub fields: Vec<String>,
        pub domains: TestDomains,
        pub view: Option<(Range<usize>, Range<usize>)>,
        pub highlights: Vec<MatrixLocation>,
        pub clusters: Vec<TestCluster>,
        pub row_splits: Vec<usize>,
        pub col_splits: Vec<usize>,
    }
    impl TestData {
        pub fn data_4_3() -> Self {
            // Create cells.

            let cells = vec![
                // row 0
                None,
                Some(vec![("foo", -1.0), ("bar", 2.0), ("baz", 3.0)]),
                Some(vec![("baz", 1.5), ("quux", 3.0)]),
                None,
                // row 1
                Some(vec![("quux", 2.8)]),
                None,
                None,
                Some(vec![("bar", 1.0)]),
                // row 2
                Some(vec![("foo", -0.5), ("quux", 4.0)]),
                Some(vec![("baz", 6.0), ("foo", -1.0)]),
                None,
                Some(vec![("bar", 0.0)]),
            ];

            // Set the domains to some made up scale.
            let domains: TestDomains = [
                ("bar", (-1.0, 2.0)),
                ("baz", (0.0, 6.0)),
                ("foo", (-1.0, 3.0)),
                ("quux", (0.0, 5.0)),
            ]
            .into_iter()
            .map(|(k, v)| (k.to_string(), v))
            .collect();

            let values: Vec<TestCell> = cells
                .into_iter()
                .map(|value_vec| {
                    value_vec.map(|v| {
                        v.into_iter()
                            .map(|(k, v)| (k.to_string(), v))
                            .collect::<BTreeMap<_, _>>()
                    })
                })
                .collect();

            // Move cells into matrix.
            let matrix = Matrix::new(values, 4).expect("data");

            let fields = domains.keys().cloned().collect::<Vec<_>>();

            Self {
                matrix,
                fields,
                domains,
                row_splits: vec![1],
                col_splits: vec![1, 3],
                clusters: vec![
                    TestCluster(0..2, 0..2, false),
                    TestCluster(0..1, 0..1, true),
                ],
                view: None,
                highlights: vec![
                    MatrixLocation::Row(1),
                    MatrixLocation::Range(MatrixRange::new(1..3, 2..5)),
                    MatrixLocation::Coordinates(MatrixCoordinates::new(1, 3)),
                ],
            }
        }
    }

    impl Dsm<TestCell, TestCluster> for TestData {}

    impl MatrixRef<TestCell> for TestData {
        fn get<T: Into<MatrixCoordinates>>(
            &self,
            coordinates: T,
        ) -> Result<&TestCell, ratio_matrix::CoordinatesOutOfBoundsError> {
            self.matrix.get(coordinates)
        }
    }
    impl MatrixValue<TestCell> for TestData {
        fn is_occupied(&self, row: usize, col: usize) -> bool {
            self.get(MatrixCoordinates::new(row, col)).is_ok()
        }
        fn field_value<S: AsRef<str>>(&self, row: usize, col: usize, field: &S) -> Option<f64> {
            let map = self.get(MatrixCoordinates::new(row, col)).ok()?.as_ref()?;
            map.get(field.as_ref()).copied()
        }
    }

    impl Domain2D for TestData {
        fn row_range(&self) -> Range<usize> {
            match &self.view {
                Some(view) => view.0.clone(),
                None => self.matrix.row_range(),
            }
        }
        fn col_range(&self) -> std::ops::Range<usize> {
            match &self.view {
                Some(view) => view.1.clone(),
                None => self.matrix.col_range(),
            }
        }
    }

    impl SplitLines for TestData {
        fn row_splits(&self) -> &[usize] {
            &self.row_splits
        }
        fn col_splits(&self) -> &[usize] {
            &self.col_splits
        }
    }

    impl Clusters<TestCluster> for TestData {
        fn clusters(&self) -> &[TestCluster] {
            &self.clusters
        }
    }

    impl Highlights for TestData {
        fn locations(&self) -> &[MatrixLocation] {
            &self.highlights
        }
        fn settings(&self) -> Option<&HighlightSettings> {
            None
        }
        fn prefix(&self) -> Option<&str> {
            None
        }
    }

    /// 2D matrix of test cells.
    pub type TestMatrix = Matrix<TestCell>;

    /// Field values by field name.
    pub type TestCell = Option<BTreeMap<String, f64>>;

    impl CellValue for TestCell {
        fn field_value<S: AsRef<str>>(&self, field: &S) -> Option<f64> {
            self.as_ref()
                .and_then(|cell| cell.get(field.as_ref()).copied())
        }
    }

    /// Field names to 1D continuous domains.
    pub type TestDomains = BTreeMap<String, (f64, f64)>;

    /// Cluster type. Row range, column range, special flag.
    #[derive(Clone, Debug)]
    pub struct TestCluster(Range<usize>, Range<usize>, bool);
    impl Cluster for TestCluster {}
    impl Domain2D for TestCluster {
        fn row_range(&self) -> Range<usize> {
            self.0.clone()
        }
        fn col_range(&self) -> Range<usize> {
            self.1.clone()
        }
    }
    impl IsSpecial for TestCluster {
        fn is_special(&self) -> bool {
            self.2
        }
    }
}
