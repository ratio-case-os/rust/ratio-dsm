//! DSM figure plotting

use svg::node::element::{Definitions, Group, SVG};

use crate::clusters::ClustersComponent;
use crate::grid::GridComponent;
use crate::highlights::HighlightsComponent;
use crate::pie::PiechartsComponent;
use crate::settings::DsmSettings;
use crate::splits::SplitsComponent;
use crate::traits::{Cluster, Component, Dsm, GridViewbox, Highlights};
use crate::utils::with_prefix;

/// DSM plot component.
#[derive(Clone, Debug)]
pub struct DsmComponent {
    group: Option<Group>,
    definitions: Option<Definitions>,
    style: Option<String>,
    viewbox: (f64, f64, f64, f64),
}
impl DsmComponent {
    pub fn new<'a, D, C, Cl, S, H>(
        prefix: Option<&str>,
        data: &'a D,
        fields: &'a [S],
        settings: &DsmSettings,
        highlights: &[H],
    ) -> Self
    where
        D: Dsm<C, Cl>,
        Cl: Cluster + Clone + 'a,
        S: AsRef<str>,
        H: Highlights,
    {
        let grid = GridComponent::new(prefix, data, settings);
        let splits = SplitsComponent::new(prefix, data, data, settings);
        let clusters = ClustersComponent::new(prefix, data, settings);
        let piecharts = PiechartsComponent::new(prefix, data, fields, settings);

        let mut components: Vec<Box<dyn Component>> = vec![];

        components.push(Box::new(grid));

        for hl in highlights {
            let component = match hl.prefix() {
                Some(s) => {
                    let prefix = with_prefix(s, prefix);
                    HighlightsComponent::new(Some(prefix.as_str()), hl, data, settings)
                }
                None => HighlightsComponent::new(prefix, hl, data, settings),
            };
            components.push(Box::new(component));
        }

        components.push(Box::new(splits));
        components.push(Box::new(clusters));
        components.push(Box::new(piecharts));

        let components = components.as_slice();
        Self {
            group: components.group(),
            definitions: components.definitions(),
            style: components.style(),
            viewbox: data.viewbox(&settings.grid),
        }
    }

    /// Create a properly viewboxed SVG document of this DSM.
    pub fn document_boxed(&self) -> SVG {
        self.document(Some(self.viewbox))
    }
}

impl Component for DsmComponent {
    fn group(&self) -> Option<Group> {
        self.group.to_owned()
    }
    fn style(&self) -> Option<String> {
        self.style.to_owned()
    }
    fn definitions(&self) -> Option<Definitions> {
        self.definitions.to_owned()
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

    #[allow(unused_imports)]
    use super::*;
    use crate::settings::{PieDisplayMode, PieDivisionMode, PieSettings};
    use crate::traits::GridViewbox;
    use crate::utils::test::{write_to_examples, TestData};

    #[test]
    fn test_dsm() {
        let data = TestData::data_4_3();
        let settings = DsmSettings {
            pie: PieSettings {
                display: PieDisplayMode::Categorical,
                division: PieDivisionMode::Relative,
                ..Default::default()
            }
            .into(),
            domains: data.domains.clone().into(),
            ..Default::default()
        };

        let dsm = DsmComponent::new(
            None,
            &data,
            &data.fields,
            &settings,
            std::slice::from_ref(&data),
        );
        let doc = dsm.document(Some(data.viewbox(&settings.grid)));
        let doc_vb = dsm.document_boxed();

        assert_eq!(doc.to_string(), doc_vb.to_string());

        // Write to examples for inspection.
        write_to_examples("dsm/dsm-22.svg", &doc);
    }
}
