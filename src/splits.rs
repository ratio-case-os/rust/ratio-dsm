//! Split lines drawing.

use palette::Srgba;
use ratio_matrix::traits::Dimensions2D;
use svg::node::element::{Definitions, Group, Line, Use};
use svg::Node;

use crate::settings::DsmSettings;
use crate::traits::{Component, SplitLines};
use crate::utils::{dasharray, with_prefix};

/// Splits plot component.
#[derive(Clone, Debug)]
pub struct SplitsComponent<'a> {
    /// Grid size in pixels.
    grid_size: usize,
    /// Width of the current grid in pixels.
    width: usize,
    /// Height of the current grid in pixels.
    height: usize,
    /// Stroke color for the split lines.
    stroke: Srgba<u8>,
    /// Stroke width in pixels for the split lines.
    stroke_width: f64,
    /// Dashing pattern for the split lines.
    dash: &'a [u8],
    /// Row indices after which a split should be drawn.
    row_splits: &'a [usize],
    /// Column indices after which a split should be drawn.
    col_splits: &'a [usize],
    /// Row split line ID for re-use.
    row_id: String,
    /// Column split line ID for re-use.
    col_id: String,
}
impl<'a> SplitsComponent<'a> {
    /// Create a new splits component based on a data source that exposes the `SplitLines` trait.
    /// It is assumed that the coordinates are already adjusted for any restrictions on the viewing
    /// domain, but the lengths of the lines from the respective function argument.
    pub fn new<D, Dim>(
        prefix: Option<&str>,
        data: &'a D,
        dimensions: &Dim,
        settings: &'a DsmSettings,
    ) -> Self
    where
        D: SplitLines,
        Dim: Dimensions2D,
    {
        let grid_size = settings.grid.size;
        let width = dimensions.n_cols() * grid_size;
        let height = dimensions.n_rows() * grid_size;
        Self {
            grid_size,
            width,
            height,
            stroke: settings.grid.split_color,
            stroke_width: settings.grid.line_width,
            dash: &settings.grid.split_dash,
            row_splits: data.row_splits(),
            col_splits: data.col_splits(),
            row_id: with_prefix("split-r", prefix),
            col_id: with_prefix("split-c", prefix),
        }
    }
    /// Whether there are no splits to display.
    pub fn is_empty(&self) -> bool {
        self.row_splits.is_empty() && self.col_splits.is_empty()
    }
}

impl<'a> Component for SplitsComponent<'a> {
    fn group(&self) -> Option<Group> {
        if self.is_empty() {
            return None;
        }
        let mut group = Group::new();

        for &split in self.row_splits {
            let y = split * self.grid_size;
            group.append(
                Use::new()
                    .set("href", format!("#{}", self.row_id))
                    .set("x", 0)
                    .set("y", y),
            );
        }

        for &split in self.col_splits {
            let x = split * self.grid_size;
            group.append(
                Use::new()
                    .set("href", format!("#{}", self.col_id))
                    .set("x", x)
                    .set("y", 0),
            )
        }

        Some(group)
    }
    fn definitions(&self) -> Option<Definitions> {
        if self.is_empty() {
            return None;
        }
        let mut defs = Definitions::new();
        if !self.row_splits.is_empty() {
            defs.append(
                Line::new()
                    .set("id", self.row_id.as_ref())
                    .set("x0", 0)
                    .set("x1", self.width)
                    .set("y0", 0)
                    .set("y1", 0)
                    .set("stroke", format!("#{:02x}", self.stroke))
                    .set("stroke-width", self.stroke_width)
                    .set("stroke-dasharray", dasharray(self.dash)),
            );
        }
        if !self.col_splits.is_empty() {
            defs.append(
                Line::new()
                    .set("id", self.col_id.as_ref())
                    .set("x0", 0)
                    .set("x1", 0)
                    .set("y0", 0)
                    .set("y1", self.height)
                    .set("stroke", format!("#{:02x}", self.stroke))
                    .set("stroke-width", self.stroke_width)
                    .set("stroke-dasharray", dasharray(self.dash)),
            )
        }
        Some(defs)
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

    #[allow(unused_imports)]
    use super::*;
    use crate::settings::DsmSettings;
    use crate::traits::{Component, GridViewbox};
    use crate::utils::test::{write_to_examples, TestData};

    #[test]
    fn test_splits() {
        let data = TestData::data_4_3();
        let settings = DsmSettings::default();

        let doc = SplitsComponent::new(None, &data, &data, &settings)
            .document(Some(data.viewbox(&settings.grid)));

        write_to_examples("splits/splits.svg", &doc);
        assert_str_eq!(
            doc.to_string(),
            r##"<svg height="100%" viewBox="-0.5 -0.5 121 91" width="100%" xmlns="http://www.w3.org/2000/svg">
<defs>
<line id="split-r" stroke="#848499ff" stroke-dasharray="10 10" stroke-width="1" x0="0" x1="120" y0="0" y1="0"/>
<line id="split-c" stroke="#848499ff" stroke-dasharray="10 10" stroke-width="1" x0="0" x1="0" y0="0" y1="90"/>
</defs>
<g>
<use href="#split-r" x="0" y="30"/>
<use href="#split-c" x="30" y="0"/>
<use href="#split-c" x="90" y="0"/>
</g>
</svg>"##
        );
    }
}
