//! Traits to define generic behavior for most function arguments.
use ratio_matrix::traits::{Dimensions2D, Domain2D, MatrixRef};
use ratio_matrix::{MatrixCellRef, MatrixCoordinates, MatrixLocation};
use svg::node::element::{Definitions, Group, Style};
use svg::{Document, Node};

use crate::settings::{GridSettings, HighlightSettings};

/// Plot component behavior.
pub trait Component {
    /// Component as an SVG group. May return none if there's nothing to draw.
    fn group(&self) -> Option<Group> {
        None
    }
    /// Relevant style for this component. May return none if there's nothing to style.
    fn style(&self) -> Option<String> {
        None
    }
    /// Relevant definitions for this component.
    fn definitions(&self) -> Option<Definitions> {
        None
    }
    /// Component as an SVG group with the style and definitions embedded.
    fn embedded_group(&self) -> Option<Group> {
        let mut group = self.group()?;
        if let Some(style) = self.style() {
            group.append(Style::new(style));
        }
        if let Some(defs) = self.definitions() {
            group.append(defs);
        }
        Some(group)
    }
    /// Component as an SVG document.
    fn document(&self, viewbox: Option<(f64, f64, f64, f64)>) -> Document {
        let mut doc = Document::new().set("width", "100%").set("height", "100%");
        if let Some(viewbox) = viewbox {
            doc.assign("viewBox", viewbox);
        }
        if let Some(style) = self.style() {
            doc.append(Style::new(style));
        }
        if let Some(defs) = self.definitions() {
            doc.append(defs);
        }
        if let Some(group) = self.group() {
            doc.append(group);
        }
        doc
    }
}
impl<C> Component for &[C]
where
    C: Component,
{
    fn group(&self) -> Option<Group> {
        let acc = self.iter().filter_map(|c| c.group()).collect::<Vec<_>>();
        if acc.is_empty() {
            None
        } else {
            let mut group = Group::new();
            for g in acc.into_iter() {
                group.append(g);
            }
            Some(group)
        }
    }
    fn style(&self) -> Option<String> {
        let acc = self.iter().filter_map(|c| c.style()).collect::<String>();
        if acc.is_empty() {
            None
        } else {
            Some(acc)
        }
    }
    fn definitions(&self) -> Option<Definitions> {
        let acc = self
            .iter()
            .filter_map(|c| c.definitions())
            .collect::<Vec<_>>();
        if acc.is_empty() {
            None
        } else {
            let mut defs = Definitions::new();
            for d in acc.into_iter() {
                for c in d.get_children() {
                    defs.append(c.to_owned());
                }
            }
            Some(defs)
        }
    }
}
impl<'a> Component for &[Box<dyn Component + 'a>] {
    fn group(&self) -> Option<Group> {
        let acc = self.iter().filter_map(|c| c.group()).collect::<Vec<_>>();
        if acc.is_empty() {
            None
        } else {
            let mut group = Group::new();
            for g in acc.into_iter() {
                group.append(g);
            }
            Some(group)
        }
    }
    fn style(&self) -> Option<String> {
        let acc = self.iter().filter_map(|c| c.style()).collect::<String>();
        if acc.is_empty() {
            None
        } else {
            Some(acc)
        }
    }
    fn definitions(&self) -> Option<Definitions> {
        let acc = self
            .iter()
            .filter_map(|c| c.definitions())
            .collect::<Vec<_>>();
        if acc.is_empty() {
            None
        } else {
            let mut defs = Definitions::new();
            for d in acc.into_iter() {
                for c in d.get_children() {
                    defs.append(c.to_owned());
                }
            }
            Some(defs)
        }
    }
}
impl Component for Group {
    fn group(&self) -> Option<Group> {
        Some(self.to_owned())
    }
}

/// All data for a 2D figure.
///
/// Note that the Domain2D trait also includes "offsets" in the 2D space by which to filter the data
/// in the other sources.
pub trait Dsm<C, Cl>: Domain2D + MatrixRef<C> + MatrixValue<C> + Clusters<Cl> + SplitLines
where
    Cl: Cluster,
{
}

/// 2D matrix cell value fetching behavior.
pub trait MatrixValue<C>: MatrixRef<C> {
    /// Whether this cell is occupied.
    #[allow(unused)]
    fn is_occupied(&self, row: usize, col: usize) -> bool {
        true
    }
    /// Get a field value from a cell in the matrix.
    fn field_value<S: AsRef<str>>(&self, row: usize, col: usize, field: &S) -> Option<f64>;

    /// Get a cet a cell reference object with a reference to this matrix as well as the coordinates.
    fn reference_cell(&self, row: usize, col: usize) -> Option<MatrixCellRef<'_, Self, C>> {
        if self.is_occupied(row, col) {
            Some(MatrixCellRef::new(self, MatrixCoordinates::new(row, col)))
        } else {
            None
        }
    }
}

/// Cell data behavior. It is recommended to implement the `MatrixValue` trait and friends on your matrix' data container instead.
pub trait CellValue {
    /// Get value for the given field.
    fn field_value<S: AsRef<str>>(&self, field: &S) -> Option<f64>;
}
impl<'a, M, C> CellValue for MatrixCellRef<'a, M, C>
where
    M: MatrixRef<C> + MatrixValue<C>,
{
    fn field_value<S: AsRef<str>>(&self, field: &S) -> Option<f64> {
        self.matrix
            .field_value(self.coordinates.row, self.coordinates.col, field)
    }
}
impl CellValue for () {
    fn field_value<S: AsRef<str>>(&self, _field: &S) -> Option<f64> {
        None
    }
}

/// An SVG viewbox as (x0, y0, x1, y1) for a given grid size.
pub trait GridViewbox {
    /// A proper viewbox that takes into account the bleeding line widths at both ends of the figure.
    fn viewbox(&self, settings: &GridSettings) -> (f64, f64, f64, f64);
}
impl<D: Dimensions2D> GridViewbox for D {
    fn viewbox(&self, settings: &GridSettings) -> (f64, f64, f64, f64) {
        let half = 0.5 * settings.line_width;
        (
            -half,
            -half,
            (self.n_cols() * settings.size) as f64 + settings.line_width,
            (self.n_rows() * settings.size) as f64 + settings.line_width,
        )
    }
}

pub trait Foo<D> {
    fn foo(&self) -> &[D];
}

/// Cluster domains ranging from coarse to fine to draw finest clusters last.
/// Each cluster exposes a flag whether they are "special".
pub trait Clusters<Cl> {
    /// Clusters to draw. The inclusion of clusters and their ranges should already be adjusted for
    /// the current viewbox.
    fn clusters(&self) -> &[Cl];
}

/// Cluster trait. A domain and whether it is a special case.
/// The domain should already be adjusted for the current viewbox.
pub trait Cluster: Domain2D + IsSpecial {}

/// Special flag.
pub trait IsSpecial {
    /// Whether this is a special instance.
    fn is_special(&self) -> bool;
}

/// Split lines that "cut up" the figure.
pub trait SplitLines {
    /// Row cells after which to draw a split line.
    fn row_splits(&self) -> &[usize];
    /// Column cells after which to draw a split line.
    fn col_splits(&self) -> &[usize];
}
impl SplitLines for (&[usize], &[usize]) {
    fn row_splits(&self) -> &[usize] {
        self.0
    }
    fn col_splits(&self) -> &[usize] {
        self.1
    }
}

/// Highlight locations.
pub trait Highlights {
    /// Highlight locations.
    fn locations(&self) -> &[MatrixLocation];

    /// Optional settings override.
    fn settings(&self) -> Option<&HighlightSettings>;

    /// Additional prefix to add to distinguish between multiple highlight instances.
    fn prefix(&self) -> Option<&str>;
}
