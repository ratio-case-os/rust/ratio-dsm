//! Piechart plotting

use std::collections::BTreeMap;
use std::f64::consts::PI;
use std::marker::PhantomData;

use palette::Srgba;
use ratio_color::{ColorAtFraction, Container};
use ratio_matrix::traits::{Dimensions2D, MatrixRef};
use ratio_matrix::MatrixCellRef;
use svg::node::element::path::Data;
use svg::node::element::{Circle, Definitions, Group, Path, Use};
use svg::Node;

use crate::settings::{DsmSettings, PaletteSettings, PieDisplayMode, PieDivisionMode, PieSettings};
use crate::traits::{CellValue, Component, MatrixValue};
use crate::utils::with_prefix;

/// Full circle in radians.
const DOUBLE_PI: f64 = 2.0 * PI;

/// Pie-chart matrix components.
#[derive(Clone, Debug)]
pub struct PiechartsComponent<'a, D, C, S>
where
    D: Dimensions2D + MatrixRef<C> + MatrixValue<C>,
    S: AsRef<str>,
{
    data: &'a D,
    _cell: PhantomData<C>,
    fields: &'a [S],
    pie: &'a PieSettings,
    palette: &'a PaletteSettings,
    domains: &'a BTreeMap<String, (f64, f64)>,
    grid_size: f64,
    radius: f64,
    pie_class: String,
    bin_id: String,
}

impl<'a, D, C, S> PiechartsComponent<'a, D, C, S>
where
    D: Dimensions2D + MatrixRef<C> + MatrixValue<C>,
    S: AsRef<str>,
{
    pub fn new(
        prefix: Option<&str>,
        data: &'a D,
        fields: &'a [S],
        settings: &'a DsmSettings,
    ) -> Self {
        let radius = settings.grid.size as f64 * settings.pie.radius;
        Self {
            data,
            _cell: PhantomData,
            fields,
            pie: &settings.pie,
            palette: &settings.palette,
            domains: &settings.domains,
            grid_size: settings.grid.size as f64,
            radius,
            pie_class: with_prefix("pie", prefix),
            bin_id: with_prefix("bin", prefix),
        }
    }
}

impl<'a, D, C, S> Component for PiechartsComponent<'a, D, C, S>
where
    D: Dimensions2D + MatrixRef<C> + MatrixValue<C>,
    S: AsRef<str>,
{
    fn group(&self) -> Option<Group> {
        let mut group = Group::new().set("class", self.pie_class.as_ref());
        let bin_ref = format!("#{}", &self.bin_id);
        let bin_ref = bin_ref.as_str();
        for pie in (0..self.data.n_rows()).flat_map(move |row| {
            let y = (row as f64 + 0.5) * self.grid_size;
            (0..self.data.n_cols()).filter_map(move |col| {
                self.data.reference_cell(row, col).map(|cell| {
                    let x = (col as f64 + 0.5) * self.grid_size;
                    piechart(
                        &cell,
                        x,
                        y,
                        self.radius,
                        self.fields,
                        self.pie,
                        self.palette,
                        self.domains,
                        None,
                        bin_ref,
                    )
                })
            })
        }) {
            group.append(pie);
        }
        Some(group)
    }
    fn definitions(&self) -> Option<Definitions> {
        match self.pie.display {
            PieDisplayMode::Binary => Some(
                Definitions::new().add(
                    Circle::new()
                        .set("id", self.bin_id.as_ref())
                        .set("fill", format!("#{:02x}", self.palette.binary))
                        .set("cx", 0)
                        .set("cy", 0)
                        .set("r", self.radius),
                ),
            ),
            _ => None,
        }
    }
    fn style(&self) -> Option<String> {
        Some(format!(
            ".pie{{stroke:#{:02x};stroke-width:{};}}",
            self.pie.stroke_color, self.pie.stroke_width
        ))
    }
}

/// Create a single piechart instance as an SVG group.
#[allow(clippy::too_many_arguments)]
fn piechart<D, C, S>(
    cell: &MatrixCellRef<D, C>,
    x: f64,
    y: f64,
    radius: f64,
    fields: &[S],
    pie: &PieSettings,
    palette: &PaletteSettings,
    domains: &BTreeMap<String, (f64, f64)>,
    scaler: Option<WedgeScaler>,
    bin_ref: &str,
) -> Group
where
    D: MatrixRef<C> + MatrixValue<C>,
    S: AsRef<str>,
{
    match pie.display {
        PieDisplayMode::Binary => binary_group(x, y, bin_ref),
        _ => wedge_group(cell, x, y, radius, fields, pie, palette, domains, scaler),
    }
}

/// Create a binary circle.
fn binary_group(x: f64, y: f64, bin_ref: &str) -> Group {
    Group::new().add(Use::new().set("href", bin_ref).set("x", x).set("y", y))
}

/// Create a categorical piechart.
#[allow(clippy::too_many_arguments)]
fn wedge_group<D, C, S>(
    cell: &MatrixCellRef<D, C>,
    x: f64,
    y: f64,
    radius: f64,
    fields: &[S],
    pie: &PieSettings,
    palette: &PaletteSettings,
    domains: &BTreeMap<String, (f64, f64)>,
    scaler: Option<WedgeScaler>,
) -> Group
where
    D: MatrixRef<C> + MatrixValue<C>,
    S: AsRef<str>,
{
    let mut group = Group::new();
    let wedges = WedgeIterator::new(cell, x, y, radius, fields, pie, palette, domains, scaler);
    for wedge in wedges {
        if wedge.start == 0.0 && wedge.end == DOUBLE_PI {
            let circle: Circle = wedge.into();
            group.append(circle);
        } else {
            let path: Path = wedge.into();
            group.append(path);
        }
    }
    group
}

/// Calculates wedges from a cell, given fields and settings.
#[derive(Clone, Debug)]
struct WedgeIterator<'a, S>
where
    S: AsRef<str>,
{
    /// X-coordinate in pixels.
    x: f64,
    /// Y-coordinate in pixels.
    y: f64,
    /// Radius in pixels.
    radius: f64,
    /// Field names or keys.
    fields: &'a [S],
    /// Field values corresponding to the ordering in `fields`.
    values: Vec<Option<f64>>,
    /// Pre-calculated wedge scaler for this piechart.
    scaler: WedgeScaler,
    /// Current index that is being iterated over.
    index: usize,
    /// Display mode, i.e. binary/categorical/numerical.
    display: &'a PieDisplayMode,
    /// Color palette to use.
    palette: &'a PaletteSettings,
    /// Color domains as they should be used in this drawing cycle.
    domains: &'a BTreeMap<String, (f64, f64)>,
    /// Starting angle for the next wedge.
    start: f64,
}
impl<'a, S> WedgeIterator<'a, S>
where
    S: AsRef<str>,
{
    /// Create a new wedge iterator.
    #[allow(clippy::too_many_arguments)]
    fn new<D, C>(
        cell: &'a MatrixCellRef<D, C>,
        x: f64,
        y: f64,
        radius: f64,
        fields: &'a [S],
        pie: &'a PieSettings,
        palette: &'a PaletteSettings,
        domains: &'a BTreeMap<String, (f64, f64)>,
        scaler: Option<WedgeScaler>,
    ) -> Self
    where
        D: MatrixRef<C> + MatrixValue<C>,
    {
        let values = fields
            .iter()
            .map(|f| cell.field_value(f))
            .collect::<Vec<_>>();
        let scaler = scaler.unwrap_or_else(|| WedgeScaler::new(&pie.division, fields, &values));
        Self {
            x,
            y,
            radius,
            fields,
            values,
            scaler,
            index: 0,
            display: &pie.display,
            palette,
            domains,
            start: 0.0,
        }
    }

    /// Calculate a wedge's angle start and ending angle.
    fn angle_domain(&self, value: f64, index: usize) -> (f64, f64) {
        match self.scaler {
            WedgeScaler::Fixed { size } => {
                let index = index as f64;
                (index * size, (index + 1.0) * size)
            }
            WedgeScaler::Equal { size } => (self.start, self.start + size),
            WedgeScaler::Relative { scale } => (self.start, self.start + scale * value.abs()),
        }
    }
}
impl<'a, S> Iterator for WedgeIterator<'a, S>
where
    S: AsRef<str>,
{
    type Item = Wedge;
    fn next(&mut self) -> Option<Self::Item> {
        let index = self.index;
        let field = self.fields.get(index)?;
        self.index += 1;

        if let Some(&value) = self.values.get(index)?.as_ref() {
            let angles = self.angle_domain(value, index);
            if angles.1 > angles.0 {
                self.start = angles.1;
                // Get the weight domain to potentially interpolate the color.
                let domain = self
                    .domains
                    .get(field.as_ref())
                    .copied()
                    .unwrap_or_default();
                return Some(Wedge {
                    x: self.x,
                    y: self.y,
                    start: angles.0,
                    end: angles.1,
                    radius: self.radius,
                    color: wedge_color(field, index, value, self.display, self.palette, domain),
                });
            }
        }
        self.next()
    }
}

/// Wedge angle scaler.
#[derive(Clone, Debug)]
pub enum WedgeScaler {
    /// Fixed angle scaler. Each field occupies the same size at its own fixed unique position.
    /// Used in conjunction with a fields index to obtain the angles.
    Fixed { size: f64 },
    /// Equal scaler. Any nonzero value gets this size.
    /// Used in conjunction with the previous end-angle to find the next while drawing.
    Equal { size: f64 },
    /// Relative scaler. Any nonzero value should be multiplied by the contained scale for sizing.
    /// Used in conjunction with the previous end-angle to find the next while drawing.
    Relative { scale: f64 },
}
impl WedgeScaler {
    /// Create a new ad-hoc wedge scaler.
    fn new<S>(division: &PieDivisionMode, fields: &[S], values: &[Option<f64>]) -> Self
    where
        S: AsRef<str>,
    {
        match division {
            PieDivisionMode::Fixed => Self::Fixed {
                size: DOUBLE_PI / fields.len() as f64,
            },
            PieDivisionMode::Equal => {
                let nonzero: usize = values.iter().fold(0, |acc, &value| match value {
                    Some(x) if x != 0.0 => acc + 1,
                    _ => acc,
                });
                match nonzero {
                    1 => Self::Equal { size: DOUBLE_PI },
                    2.. => Self::Equal {
                        size: DOUBLE_PI / nonzero as f64,
                    },
                    _ => Self::Equal { size: 0.0 },
                }
            }
            PieDivisionMode::Relative => {
                let magnitude = values.iter().fold(0.0, |acc, &value| match value {
                    Some(v) => acc + v.abs(),
                    None => acc,
                });
                if magnitude > 0.0 {
                    Self::Relative {
                        scale: DOUBLE_PI / magnitude,
                    }
                } else {
                    Self::Relative { scale: 0.0 }
                }
            }
        }
    }
    /// Precalculate the scalar if possible.
    pub fn precalculate(division: &PieDivisionMode, fields: &[String]) -> Option<Self> {
        if division == &PieDivisionMode::Fixed {
            Some(Self::Fixed {
                size: DOUBLE_PI / fields.len() as f64,
            })
        } else {
            None
        }
    }
}

/// Get a wedges color by field and value.
fn wedge_color<S: AsRef<str>>(
    field: S,
    index: usize,
    value: f64,
    display: &PieDisplayMode,
    palette: &PaletteSettings,
    domain: (f64, f64),
) -> Srgba<u8> {
    match display {
        PieDisplayMode::Binary => palette.binary,
        PieDisplayMode::Categorical => palette
            .categorical
            .get(field.as_ref())
            .or_else(|| palette.categorical.by_index(index))
            .copied()
            .unwrap_or_default(),
        PieDisplayMode::Numerical => {
            let fraction = if domain.1 > domain.0 {
                (value.min(domain.1) - domain.0).max(0.0) / (domain.1 - domain.0)
            } else {
                1.0
            };
            palette
                .numerical
                .get(field.as_ref())
                .or_else(|| palette.numerical.by_index(index))
                .map(|gradient| gradient.color_at(fraction))
                .unwrap_or_default()
        }
    }
}

/// Data to draw a wedge.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(serde::Serialize, serde::Deserialize),
    serde(default, rename_all = "camelCase")
)]
pub struct Wedge {
    /// X-coordinate in pixels.
    pub x: f64,
    /// Y-coordinate in pixels.
    pub y: f64,
    /// Start angle in radians.
    pub start: f64,
    /// End angle in radians.
    pub end: f64,
    /// Radius in pixels.
    pub radius: f64,
    /// Fill color.
    pub color: Srgba<u8>,
}
impl Default for Wedge {
    fn default() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            radius: 1.0,
            start: 0.0,
            end: 2.0 * PI,
            color: Srgba::new(99, 110, 250, 255),
        }
    }
}

impl From<Wedge> for Path {
    fn from(val: Wedge) -> Self {
        let Wedge {
            x,
            y,
            start,
            end,
            radius,
            color,
        } = val;
        let large: u8 = if (end - start) > PI { 1 } else { 0 };
        let data = Data::new()
            .move_to((x, y))
            .line_to((x + radius * f64::cos(start), y - radius * f64::sin(start)))
            .elliptical_arc_to((
                radius,
                radius,
                0.0,
                large,
                0,
                x + radius * f64::cos(end),
                y - radius * f64::sin(end),
            ))
            .close();
        Path::new()
            .set("d", data)
            .set("fill", format!("#{:02x}", color))
    }
}

impl From<Wedge> for Circle {
    fn from(val: Wedge) -> Self {
        let Wedge { x, y, color, .. } = val;
        Circle::new()
            .set("cx", x)
            .set("cy", y)
            .set("r", val.radius)
            .set("fill", format!("#{:02x}", color))
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use strum::IntoEnumIterator;

    #[allow(unused_imports)]
    use super::*;
    use crate::settings::PieSettings;
    use crate::traits::GridViewbox;
    use crate::utils::test::{write_to_examples, TestData};

    #[test]
    fn test_piechart() {
        let data = TestData::data_4_3();

        // Iterate over all drawing modes to generate every possible piechart with these settings.
        for display in PieDisplayMode::iter() {
            // if display != PieDisplayMode::Categorical {
            //     continue;
            // }
            for division in PieDivisionMode::iter() {
                // if division != PieDivisionMode::Equal {
                //     continue;
                // }
                // Some default settings.
                let settings = DsmSettings {
                    pie: PieSettings {
                        display,
                        division,
                        ..Default::default()
                    }
                    .into(),
                    domains: data.domains.clone().into(),
                    ..Default::default()
                };

                // Create a single piechart document.
                // Binary ones will be empty, since they re-use a definition that won't be there in the document.
                let doc = piechart(
                    &data.reference_cell(0, 1).unwrap(),
                    settings.grid.size as f64 * 0.5,
                    settings.grid.size as f64 * 0.5,
                    settings.pie.radius * settings.grid.size as f64,
                    &data.fields,
                    &settings.pie,
                    &settings.palette,
                    &settings.domains,
                    None,
                    "#bin",
                )
                .document(Some(data.viewbox(&settings.grid)));

                // Write to examples for inspection.
                write_to_examples(
                    &format!("piechart/piechart-{:?}-{:?}.svg", display, division),
                    &doc,
                );

                // Create a grid piechart document.
                let doc = PiechartsComponent::new(None, &data, &data.fields, &settings)
                    .document(Some(data.viewbox(&settings.grid)));

                // Write to examples for inspection.
                write_to_examples(
                    &format!("piecharts/piecharts-{:?}-{:?}.svg", display, division),
                    &doc,
                );
            }
        }
    }
}
